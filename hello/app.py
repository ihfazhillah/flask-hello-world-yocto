from flask import Flask


def main():
    app = Flask(__name__)

    @app.route('/')
    def hello():
        return 'hello world'

    @app.route('/greet/<name>')
    def greet(name):
        return f'hello {name}'

    app.run(host='0.0.0.0', port=5000)


if __name__ == "__main__":
    main()
