import setuptools

setuptools.setup(
    name="hello",
    version="0.0.1",
    author="ihfazhillah",
    author_email="mihfazhillah@gmail.com",
    description="a sample of flask app to work with yocto",
    packages=["hello"],
    install_requires=["flask"],
    python_requires=">=3.6",
    entry_points={"console_scripts": {"run = hello.app:main"}},
)
